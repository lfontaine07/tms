
```bash
export TEXINPUTS="${PWD}/sld/thm/;${TEXINPUTS}"
pandoc --highlight-style kate -t beamer -o 00cours.pdf 00cours.md
```

- https://ashwinschronicles.github.io/beamer-slides-using-markdown-and-pandoc
- https://github.com/alexeygumirov/pandoc-beamer-how-to


Example

```
---
title:
- Une introduction à la programmation \newline avec py5 (python)
author:
- tahiry razafindralambo
theme:
- metropolis
date:
- Semestre 1, 2023
aspectratio: 
- 169
fontsize: 
- 12pt
slide-level: 
- 2
header-includes: |
    \setbeamercolor{frametitle}{bg=black,fg=white} 
fonttheme: 
- professionalfonts
mainfont: 
- Hack Nerd Font
---


# titre
## ce que nous allons faire. {.t}

![](img/00.png){ width=250px }
![](img/aleph0.png){height=.5}

:::: {.column width=.6}


| **Item** | **Option** |
|:---------|:----------:|
| Item 1   | Option 1   |
| Item 2   | Option 2   |

```

https://training.play-with-docker.com/ops-stage1/
https://training.play-with-docker.com/ops-s1-hello/