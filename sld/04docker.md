---
title:
- Outils pour l'ingénieur \newline docker (2)
author:
- tahiry razafindralambo
theme:
- metropolis
date:
- Semestre 1, 2023
aspectratio: 
- 169
fontsize: 
- 12pt
slide-level: 
- 2
header-includes: |
    \setbeamercolor{frametitle}{bg=black,fg=white} 
---


# docker (un peu de pratique)


## Installation

- installer docker selon votre système d'exploitation
- sous windows wsl2 fonctionne bien (il semblerait)

\vspace{1cm}

- si vous ne voulez pas faire d'installation, il y a un playground ici:

    https://labs.play-with-docker.com
    
    il faut créer un compte

# tutoriels et exemples



## hello world

\tiny
::: columns
:::: {.column width=.4}




```bash
$ docker run hello-world
[...]
```

::::

:::: {.column width=.6}

![](img/docker03.png)

::::
:::
\normalsize


## another linux system

\tiny
::: columns
:::: {.column width=.4}


```bash
$ docker image pull alpine
$ docker image ls
$ docker run alpine ls -l
$ docker run alpine echo "hello from alpine"
"hello from alpine"
$ docker run alpine /bin/sh
$ docker run -it alpine /bin/sh
/ # ls
/ # exit
```

::::

:::: {.column width=.6}

![](img/docker04.png)

::::
:::
\normalsize


## lister les conteneurs

\tiny
::: columns
:::: {.column width=.4}


```bash
$ docker container ls
$ docker container ls -a
```

::::

:::: {.column width=.6}

![](img/docker05.png)

::::
:::
\normalsize

## isolation des conteneurs

\tiny
::: columns
:::: {.column width=.4}


```bash
$ docker container run -it alpine /bin/ash
/ # echo "toto" > test.txt
/ # ls
/ # exit
$ docker container run alpine ls
[... surprise ...]
$ docker container ls -a
```

::::

:::: {.column width=.6}

![](img/docker06.png)

::::
:::
\normalsize

## instance de conteneur

\tiny
::: columns
:::: {.column width=.4}


```bash
$ docker container ls -a
$ docker container start <container ID>
$ docker container ls
$ docker container exec <container ID> ls
```

::::

:::: {.column width=.6}

![](img/docker07.png)

::::
:::
\normalsize

# un peu plus avec docker

## creation d'une image (1)

\tiny
::: columns
:::: {.column width=.4}


```bash
$ docker container run -ti ubuntu bash
# / apt update && apt install -y figlet
# / figlet "hello WOLRD !!!"
# / exit

$ docker container ls -a
$ docker container commit CONTAINER_ID

$ docker container start <container ID>
$ docker container ls
$ docker container exec <container ID> ls
$ docker image ls
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
<none>              <none>              a104f9ae9c37        46 seconds ago      160MB
ubuntu              latest              14f60031763d        4 days ago          120MB
```

::::

:::: {.column width=.6}

- commit permet de créer une image localement. 

::::
:::
\normalsize


## creation d'une image (2)

\tiny
::: columns
:::: {.column width=.4}


```bash
$ docker image tag <IMAGE_ID> figlet
$ docker image ls
(...)

$ docker container run figlet figlet hello
```

::::

:::: {.column width=.6}

![](img/docker08.png)

::::
:::
\normalsize

## creation d'une image (3)

\tiny
::: columns
:::: {.column width=.4}


```bash
$ vim index.js
 var os = require("os");
 var hostname = os.hostname();
 console.log("hello from " + hostname);
$ vim Dockerfile
 FROM alpine
 RUN apk update && apk add nodejs
 COPY . /app
 WORKDIR /app
 CMD ["node","index.js"]
$ docker image build -t hello:v0.1 .
$ docker container run hello:v0.1


```

::::

:::: {.column width=.6}

![](img/docker09.png)

::::
:::
\normalsize

## couche ou layers

\tiny
::: columns
:::: {.column width=.4}


```bash
$ docker image history <image ID>
$ echo "console.log(\"this is v0.2\");" >> index.js
$ docker image build -t hello:v0.2 .
 (utilisation d'un cache)


```

::::

:::: {.column width=.6}

- durant un build, il y a plusieurs étapes avec des `fetch` et des `pull`. Ces étapes créent des couches (layers).
- l'utilisation des `layers` et du `cache` permet d'optimiser les `builds`
- 

::::
:::
\normalsize

## inspection d'image

\tiny
::: columns
:::: {.column width=.4}


```bash
$ docker image pull alpine
$ docker image inspect alpine
```

::::

:::: {.column width=.6}

- permet de voir en détails ce qui est contenu dans l'image

::::
:::
\normalsize


# le réseau dans docker

## commandes de base pour le réseau

\tiny
::: columns
:::: {.column width=.4}


```bash
$ docker network
$ docker network ls
    [...]
$ docker network inspect bridge
    [...]
$ docker info
```

::::

:::: {.column width=.6}

- `docker network` affiche l'aide pour la commande
- `docker network ls` liste les réseaux de conteneurs sur l'hôte
- `docker network inspect bridge` détail sur l'interface bridge
- `docker info` information sur l'installation docker et plus particulièrement sur les plugins réseaux

::::
:::
\normalsize

## le réseau bridge

\tiny
::: columns
:::: {.column width=.4}


```bash
$ docker network ls
$ brctl show
    bridge name	bridge id		STP enabled	interfaces
    docker0		8000.024252ed52f7	no
$ ifconfig
    [...]
    docker0 Link encap:Eternet HWaddr 02:42:A0:66:59:88
    inet addr:172.17.0.1 Bcast:172.17.255.255 Mask:255.255.0.0 
    [...]

```
::::

:::: {.column width=.6}

- la commande `brctl` est nécessaire sur l'hôte.
- `docker0` est un bridge crée automatiquement à l'installation de docker
- `docker0` n'a pas pour le moment d'interface connecté
- sur l'hôte, l'interface `docker0` possède une IP

::::
:::
\normalsize


## connecter un conteneur

\tiny
::: columns
:::: {.column width=.9}


```bash
$ docker run -dt ubuntu sleep infinity
    [...]
$ docker ps
    [...] un conteneur fonctionne [...]
$ brctl show
    [...] interface connecté [...]
$ docker network inspect bridge
    [...] le nouveau conteneur est attaché au bridge [...]
        "Containers": {
            "846af8479944d406843c90a39cba68373c619d1feaa932719260a5f5afddbf71": {
                "Name": "heuristic_boyd",
                "EndpointID": "1265c418f0b812004d80336bafdc4437eda976f166c11dbcc97d365b2bfa91e5",
                "MacAddress": "02:42:ac:11:00:02",
                "IPv4Address": "172.17.0.2/16",
                "IPv6Address": ""
            }
        },
    [...]

```
::::

:::: {.column width=.1}



::::
:::
\normalsize


## teste de la connectivité

\tiny
::: columns
:::: {.column width=.9}


```bash
$ ping -c2 172.17.0.2
    PING 172.17.0.2 (172.17.0.2) 56(84) bytes of data.
    64 bytes from 172.17.0.2: icmp_seq=1 ttl=64 time=0.055 ms
    64 bytes from 172.17.0.2: icmp_seq=2 ttl=64 time=0.031 ms

    --- 172.17.0.2 ping statistics ---
    2 packets transmitted, 2 received, 0% packet loss, time 4075ms
    rtt min/avg/max/mdev = 0.031/0.041/0.055/0.011 ms
$ docker ps
$ docker exec -it yourcontainerid /bin/bash
ubuntu $ ping -c2 www.google.com
    64 bytes from 104.239.220.248: icmp_seq=1 ttl=45 time=38.1 ms
    64 bytes from 104.239.220.248: icmp_seq=2 ttl=45 time=37.3 ms
    
    --- www.google.com ping statistics ---
    2 packets transmitted, 2 received, 0% packet loss, time 4003ms
    rtt min/avg/max/mdev = 37.372/37.641/38.143/0.314 ms
ubuntu $ exit
```
::::

:::: {.column width=.1}



::::
:::
\normalsize

## configuration nat

\tiny
::: columns
:::: {.column width=.9}


```bash
$ docker run --name web1 -d -p 8080:80 nginx
    [...] démarre une machine avec un server nginx [...] 
$ docker ps
    CONTAINER ID        IMAGE               COMMAND                 PORTS                           NAMES
    4e0da45b0f16        nginx               "nginx -g daemon ..."  443/tcp, 0.0.0.0:8080->80/tcp   web1
    [...] map le port 8080 sur toute les interfaces de l hôte au port 80 du conteneur [...]

$ curl 127.0.0.1:8080
    <!DOCTYPE html>
    <html>
        [...]
        <p><em>Thank you for using nginx.</em></p>
        [...]
    </html>

ubuntu $ ping -c2 www.google.com
    64 bytes from 104.239.220.248: icmp_seq=1 ttl=45 time=38.1 ms
    64 bytes from 104.239.220.248: icmp_seq=2 ttl=45 time=37.3 ms
    
    --- www.google.com ping statistics ---
    2 packets transmitted, 2 received, 0% packet loss, time 4003ms
    rtt min/avg/max/mdev = 37.372/37.641/38.143/0.314 ms
ubuntu $ exit
```
::::

:::: {.column width=.1}



::::
:::
\normalsize




## bibliographie

[1] https://training.play-with-docker.com/, 2023

\normalsize

